// Tuan Danh Huynh
// id: 2032677
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    // constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    // getter
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + "\nNumber of Gears: " + this.numberGears + "\nMax speed: " + this.maxSpeed;
    }
}